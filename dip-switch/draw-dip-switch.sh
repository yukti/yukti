#!/bin/bash
#
# yukti/dip_switch/draw-dip-switch.sh
#
# This script creates images of dip switches based on user input.
# It provides various options to customize the dip switches to
# customize the generated image.
#
#
# The MIT License
#
# Copyright (c) 2012 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# Constants
# ==============================================================================

#
# Version of the script
#
VERSION="0.80"

#
# Error: Insufficient arguments
#
E_NARGS=101

#
# Error: Incorrect argument(s)
#
E_ARGS=102

#
# Error: Couldn't generate the image file
#
E_IMAGE=103

#
# Error: Incorrect switch positions specified
#
E_SWPOS=104

# ==============================================================================
# Variables
#
# Current values indicate defaults - can be overridden via command-line
# ==============================================================================

#
# Color of the dip-switch body
#
color_body="'black'"

#
# Color of the switch
#
color_switch="'white'"

#
# Color of the switch on selected position
#
color_switch_sel="'black'"

#
# Color of the switch marked don't-care
#
color_switch_xcare="'#b0b0b0'"

#
# Color of the text on the switch body
#
color_text="'white'"

#
# Color of the title area
#
color_title_bg="'black'"

#
# Color of the title text
#
color_title_fg="'white'"

#
# Font used for switch labels
#
font_label="Liberation-Mono-Regular"

#
# Font used for the title
#
font_title="Liberation-Mono-Regular"

#
# Number of switches in the package
#
switch_num=8

#
# Width of individual switch (in pixels)
#
switch_width=30

#
# Height of individual switch (in pixels)
#
switch_height=60

#
# Horizontal spacing between switches (in pixels)
#
switch_spacing=10

#
# Margin from the image border (in pixels)
#
margin=10

#
# Image title (Usually identifies switch position on the board)
# Default: No title
#
title=

#
# Height of the title (in pixels)
#
title_height=25

#
# Draw switch labels?
# (Default: true)
#
label=1

#
# Width of the labels (in pixels)
#
label_width=30

#
# Height of the labels (in pixels)
#
label_height=15

#
# Text for ON position
# (Default: ON)
#
text_on="ON"

#
# Text for OFF position
# (Default: OFF)
#
text_on="OFF"

#
# Switch setting provided by the user to represent in the image
# (User input is saved into this array)
#
sketch=( )

#
# Image width (in pixels)
# It is calculated at run time
#
image_width=0

#
# Image height (in pixels)
# It is calculated at run time
#
image_height=0

#
# Default name of generated image file
#
image_file="dip_switch.png"

#
# List of arguments for generating the desired image.
# This list be constructed as the script progresses
#
args=""

# ==============================================================================
# Functions
# ==============================================================================

#
# Show detailed information about the script
#
function ShowInfo
{
	echo "::"
	echo ":: Simple utility to draw a dip switch"
	echo "::"
	echo ":: Author  : Sanjeev Premi (spremi-at-ymail-dot-com)"
	echo ":: License : The MIT License"
	echo "::           (http://www.opensource.org/licenses/MIT)"
	echo "::"
}

#
# Show banner
#
function ShowBanner
{
	echo "::"
	echo ":: draw-dip-switch.sh    (version ${VERSION})"
	echo "::"
}

#
# Show help message
#
function ShowHelp
{
	echo "::"
	echo ":: Usage: ${0} switch-positions [config-options]"
	echo "::"
	echo ":: where,"
	echo "::"
	echo "::    switch-positions:"
	echo " :"
	echo " :           A string representing the On/ Off/ Don't"
	echo " :           care positions in the switch delimited by"
	echo " :           '-' e.g. 1-0-1-0-0-1-2-2"
	echo " :"
	echo " :           0 = Off, 1 = On, 2 = Don't Care"
	echo " :"
	echo "::    config-options:"
	echo " :"
	echo " :           A set of options used to customize al1"
	echo " :           aspects of the image being generated."
	echo " :"
	echo " :  Here is a list of all the options:"
	echo " :"
	echo " :    Name of generated file:"
	echo " :"
	echo " :     --file=filename                (${image_file})"
	echo " :"
	echo " :    Switch attributes:"
	echo " :"
	echo " :     --switch-num=number            (${switch_num})"
	echo " :     --switch-width=number          (${switch_width})"
	echo " :     --switch-height=number         (${switch_height})"
	echo " :     --switch-spacing=number        (${switch_spacing})"
	echo " :     --label=(0, 1)                 (${label})"
	echo " :     --label-width=number           (${label_width})"
	echo " :     --label-height=number          (${label_height})"
	echo " :"
	echo " :    Colors used"
	echo " :"
	echo " :     --color-body=color             (${color_body})"
	echo " :     --color-switch=color           (${color_switch})"
	echo " :     --color-switch-sel=color       (${color_switch_sel})"
	echo " :     --color-switch-xcare=color     (${color_switch_xcare})"
	echo " :     --color-text=color             (${color_text})"
	echo " :     --color-title-bg=color         (${color_title_bg})"
	echo " :     --color-title-fg=color         (${color_title_fg})"
	echo " :"
	echo " :    Fonts used"
	echo " :"
	echo " :     --font-label=fontname          (${font_label})"
	echo " :     --font-title=fontname          (${font_title})"
	echo " :"
	echo " :    Image attributes:"
	echo " :"
	echo " :     --margin=number                (${margin})"
	echo " :     --title=text                   (${title})"
	echo " :     --title-height=number          (${title_height})"
	echo " :"
	echo "::"
}

#
# Show error message
#
function ShowError
{
	local err=$1

	local str=":: Error: "

	case $err in
		$E_NARGS)
			str+="Sufficient arguments not specified."
			;;

		$E_ARGS)
			str+="Incorrect argument(s) specified."
			;;

		$E_IMAGE)
			str+="Unable to create the image file."
			;;

		$E_SWPOS)
			str+="Incorrect switch positions specified."
			;;

		*)
			str+="Unknown error"
			;;
	esac

	echo $str
	echo "::"
	echo ""

	exit $err
}

#
# Read all command-line arguments
#
function GetArguments
{
	local ret=0
	local arg
	local name
	local value

	for arg in "$@"
	do
		name=$(echo $arg | cut -d"=" -f1)
		value=$(echo $arg | cut -d"=" -f2)

		case $name in

		"--file")
			image_file=$value
			;;

		"--switch-num")
			if [[ "$value" =~ ^[0-9]+$ ]]; then
				switch_num=$value
			else
				ret=$E_ARGS
			fi
			;;

		"--switch-width")
			if [[ "$value" =~ ^[0-9]+$ ]]; then
				switch_width=$value
			else
				ret=$E_ARGS
			fi
			;;

		"--switch-height")
			if [[ "$value" =~ ^[0-9]+$ ]]; then
				switch_height=$value
			else
				ret=$E_ARGS
			fi
			;;

		"--switch-spacing")
			if [[ "$value" =~ ^[0-9]+$ ]]; then
				switch_spacing=$value
			else
				ret=$E_ARGS
			fi
			;;

		"--label")
			if [ "$value" == "0" ] || [ "$value" == "1" ]; then
				label=$value
			else
				ret=$E_ARGS
			fi
			;;

		"--label-width")
			if [[ "$value" =~ ^[0-9]+$ ]]; then
				label_width=$value
			else
				ret=$E_ARGS
			fi
			;;

		"--label-height")
			if [[ "$value" =~ ^[0-9]+$ ]]; then
				label_height=$value
			else
				ret=$E_ARGS
			fi
			;;

		"--color-body")
			color_body=$value
			;;

		"--color-switch")
			color_switch=$value
			;;

		"--color-switch-sel")
			color_switch_sel=$value
			;;

		"--color-switch-xcare")
			color_switch_xcare=$value
			;;

		"--color-text")
			color_text=$value
			;;

		"--color-title-bg")
			color_title_bg=$value
			;;

		"--color-title-fg")
			color_title_fg=$value
			;;

		"--font-label")
			font_label=$value
			;;

		"--font-title")
			font_title=$value
			;;

		"--margin")
			if [[ "$value" =~ ^[0-9]+$ ]]; then
				margin=$value
			else
				ret=$E_ARGS
			fi
			;;

		"--title")
			title=$value
			;;

		"--title-height")
			if [[ "$value" =~ ^[0-9]+$ ]]; then
				title_height=$value
			else
				ret=$E_ARGS
			fi
			;;

		*)
			ret=$E_ARGS
			;;
		esac

		[ $ret -ne 0 ] && break
	done

	return $ret
}

#
# Check the switch positions
#
function CheckSwitchPositions
{
	local ret=0
	local j

	for (( j=0; j<${#sketch[@]}; j++ ))
	do
		[ "${sketch[$j]}" != "0" ] &&
		[ "${sketch[$j]}" != "1" ] &&
		[ "${sketch[$j]}" != "2" ] && ret=1
	done

	return $ret
}

#
# Calculate height and width of the image to be generated
#
function CalcImageWxH
{
	local str

	#
	# Calculate width
	#
	str="(${switch_num} * ${switch_width})"
	str+=" + (2 * ${margin})"
	str+=" + ((${switch_num} + 1) * ${switch_spacing})"

	if [ ${label} -eq 1 ]
	then
		str+=" + ${label_width}"
	fi

	image_width=$(echo "${str}" | bc)

	#
	# Calculate height
	#
	str="${switch_height}"
	str+=" + (2 * ${margin})"
	str+=" + (2 * ${switch_spacing})"

	if [ ${label} -eq 1 ]
	then
		str+=" + ${label_height}"
		str+=" + ${switch_spacing}"
	fi

	if [ ! -z "${title}" ]
	then
		str+=" + ${switch_spacing}"
		str+=" + ${title_height}"
	fi

	image_height=$(echo "${str}" | bc)

	args+=" -size ${image_width}x${image_height}"
	args+=" xc:white"
}

#
# Draw an switch background
#
function DrawSwitchBackground
{
	local t		# Top
	local l		# Left
	local b		# Bottom
	local r		# Right

	local str

	t=${margin}
	l=${margin}

	#
	# Calculate bottom
	#
	str="${image_height} - ${margin}"

	if [ ! -z "${title}" ]
	then
		str+=" - ${switch_spacing}"
		str+=" - ${title_height}"
	fi

	b=$(echo "${str}" | bc)

	#
	# Calculate right
	#
	str="${image_width} - ${margin}"

	r=$(echo "${str}" | bc)

	args+=" -fill ${color_body} -draw 'rectangle ${l},${t} ${r},${b}'"
}

#
# Draw an individual switch
#
function DrawSwitchPos
{
	local n=$1	# Switch number
	local v=$2	# Switch value

	local t		# Top
	local l		# Left
	local b		# Bottom
	local r		# Right
	local m		# Mid

	local str

	#
	# Draw basic switch background
	#

	# Calculate left
	str="${margin}"
	str+=" + ((${n} + 1) * ${switch_spacing})"
	str+=" + (${n} * ${switch_width})"

	l=$(echo "${str}" | bc)

	# Calculate top
	t=$(echo "(${margin} + ${switch_spacing})" | bc)

	# Calculate right
	r=$(echo "(${l} + ${switch_width})" | bc)

	# Calculate bottom
	b=$(echo "(${t} + ${switch_height})" | bc)

	# Calculate mid
	m=$(echo "(${t}+ (${switch_height} / 2))" | bc)

	# Draw rectangle
	str="stroke ${color_body}"

	if [ ${v} -ne 0 ] && [ ${v} -ne 1 ]
	then
		str+=" fill ${color_switch_xcare}"
	else
		str+=" fill ${color_switch}"
	fi

	str+=" rectangle ${l},${t} ${r},${b}"

	args+=" -strokewidth 1 -draw \"${str}\""

	# Draw the switch divider
	str="stroke ${color_body}"
	str+=" line ${l},${m} ${r},${m}'"

	args+=" -strokewidth 1 -draw \"${str}\""

	#
	# Draw switch position
	#

	# Calculate left
	l=$(echo "${l} + (${switch_width} / 5)" | bc)

	# Calculate right
	r=$(echo "${r} - (${switch_width} / 5)" | bc)

	# Calculate top
	if [ ${v} -eq 1 ]
	then
		t=$(echo "${t} + (${switch_height} / 10)" | bc)
	else
		t=$(echo "${m} + (${switch_height} / 10)" | bc)
	fi

	# Calculate bottom
	if [ ${v} -eq 1 ]
	then
		b=$(echo "${m} - (${switch_height} / 10)" | bc)
	else
		b=$(echo "${b} - (${switch_height} / 10)" | bc)
	fi

	# Draw rectangle
	str="stroke ${color_switch_sel}"
	str+=" fill ${color_switch_sel}"
	str+=" rectangle ${l},${t} ${r},${b}"

	args+=" -strokewidth 1 -draw \"${str}\""
}

#
# Draw switch number
#
function DrawSwitchNum
{
	local n=$1	# Switch number

	local t		# Top
	local l		# Left

	local str

	# Calculate left
	str="${margin}"
	str+=" + ((${n} + 1) * ${switch_spacing})"
	str+=" + (${n} * ${switch_width})"
	str+=" + (${switch_width} / 2)"

	l=$(echo "${str}" | bc)

	# Calculate top
	t=$(echo "(${margin} + ${switch_height} + (2 * ${switch_spacing}))" | bc)

	# Adjust switch number
	n=$(echo "(${n} + 1)" | bc)

	# Draw the switch number
	str="stroke ${color_text}"
	str+=" fill ${color_switch_sel}"
	str+=" gravity NorthWest"
	str+=" text ${l},${t} '${n}'"

	args+=" -strokewidth 1 -draw \"${str}\""
}

#
# Set the font to be used for the labels
#
function SetLabelFont
{
	args+=" -font '${font_label}'"
	args+=" -pointsize 14"
}

#
# Set the font to be used for the title
#
function SetTitleFont
{
	args+=" -font '${font_title}'"
	args+=" -pointsize 24"
}

#
# Draw label
#
function DrawLabel
{
	local t		# Top
	local l		# Left

	local str

	# Calculate left
	str="${margin}"
	str+=" + ((${switch_num} + 1) * ${switch_spacing})"
	str+=" + (${switch_num} * ${switch_width})"

	l=$(echo "${str}" | bc)

	# Calculate top
	t=$(echo "(${margin} + ${switch_spacing})" | bc)

	# Draw label ON
	str="stroke ${color_text}"
	str+=" gravity NorthWest"
	str+=" text ${l},${t} 'ON'"

	args+=" -strokewidth 1 -draw \"${str}\""

	# Calculate top
	t=$(echo "(${margin} + ${switch_spacing} + ((${switch_height} * 8) / 10))" | bc)

	# Draw label OFF
	str="stroke ${color_text}"
	str+=" gravity NorthWest"
	str+=" text ${l},${t} 'OFF'"

	args+=" -strokewidth 1 -draw \"${str}\""
}

#
# Draw image title
#
function DrawTitle
{
	local t		# Top
	local l		# Left
	local r		# Right
	local b		# Bottom

	local str

	# left
	l=${margin}

	# Calculate right
	r=$(echo "${image_width} - ${margin}" | bc)

	# Calculate bottom
	b=$(echo "${image_height} - ${margin}" | bc)

	# Calculate top
	str="${margin} + ${switch_height} + (2 * ${switch_spacing})"

	if [ ${label} -eq 1 ]
	then
		str+=" + ${label_height}"
		str+=" + ${switch_spacing}"
	fi

	t=$(echo "${str}" | bc)

	# Fill the title area
	str="fill ${color_title_bg} stroke ${color_title_bg}"
	str+=" rectangle ${l},${t} ${r},${b}"

	args+=" -strokewidth 1"
	args+=" -draw '${str}'"

	# Draw title
	str="gravity South"
	str+=" stroke ${color_title_fg} fill ${color_title_fg}"
	str+=" text 0,${margin} '${title}'"

	args+=" -draw \"${str}\""
}

# ==============================================================================
# Begin
# ==============================================================================

ShowBanner

#
# Check fo minimum number of arguments
#
[ $# -lt 1 ] && ShowHelp && ShowError $E_NARGS

#
# Show help (if requested)
#
if [ $# -eq 1 ] && [ "${1}" == "help" ]
then
	ShowHelp && exit 0
fi

#
# Read the specified switch positions
#
sketch=($(echo $1 | tr "-" " "))

#
# Verify that switch positions are valid
#
CheckSwitchPositions
[ $? -ne 0 ] && ShowHelp && ShowError $E_SWPOS

#
# Parse command-line arguments
#
sketch=($(echo $1 | tr "-" " "))

if [ $# -gt 1 ]
then
	shift
	GetArguments "${@}"
	[ $? -ne 0 ] && ShowHelp && ShowError $E_ARGS
fi

#
# Fill-up empty switch positions (if any)
#
if [ ${#sketch[@]} -lt ${switch_num} ]
then
	for (( i=${#sketch[@]}; i<$switch_num; i++ ))
	do
		sketch[${#sketch[*]}]=2
	done
fi

#
# Generate sequence of arguments to be passed to 'convert' for
# rendering the dip switch image.
#
CalcImageWxH

DrawSwitchBackground

for (( i=0; i<$switch_num; i++ ))
do
	DrawSwitchPos $i ${sketch[$i]}
done

if [ ${label} -eq 1 ]
then
	SetLabelFont

	for (( i=0; i<$switch_num; i++ ))
	do
		DrawSwitchNum $i
	done

	DrawLabel
fi

if [ ! -z "${title}" ]
then
	SetTitleFont
	DrawTitle
fi

#
# Generate the image
#
eval "convert ${args} ${image_file}"

[ $? -ne 0 ] && ShowError $E_IMAGE

if [ -f ${image_file} ]
then
	echo " : Created ${image_file}"
	echo "::"
else
	echo " : Couldn't create ${image_file}"
	echo "::"

	ShowError $E_IMAGE
fi

# ==============================================================================
# End
# ==============================================================================
exit 0
