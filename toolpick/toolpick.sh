#!/bin/bash
#
# yukti/toolpick/toolpick.sh
#
# This script lets developers interactively choose active toolchain,
# from a list of ones installed, to be used.
#
#
# The MIT License
#
# Copyright (c) 2014 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# Constants
# ==============================================================================

#
# Version string
#
TP_VERSION="0.80"

#
# Copyright string
#
TP_COPYRIGHT="(c) 2014 Sanjeev Premi (spremi@ymail.com)"

#
# License string
#
TP_LICENSE="This utility is distributed under the MIT License."

#
# System-level configuration file
#
CFG_SYSTEM="/etc/toolpick"

#
# User-level configuration file
#
CFG_USER="${HOME}/.toolpick"

#
# ANSI color used for the header
#
CLR_HEAD='\e[36m'

#
# ANSI color used for the footer
#
CLR_FOOT='\e[37m'

#
# ANSI color used for the menu options
#
CLR_MENU='\e[33m'

#
# ANSI color used for prompt
#
CLR_PROMPT='\e[1;35m'

#
# ANSI color used for help text
#
CLR_HELP='\e[1;37m'

#
# ANSI color used for success string
#
CLR_SUCCESS='\e[92m'

#
# ANSI color used for failure string
#
CLR_FAILURE='\e[91m'

#
# ANSI reset to default color
#
CLR_RESET='\e[0m'


# ==============================================================================
# Variables
# ==============================================================================

#
# Vertical margin (top & bottom) for the menu
#
vmargin=1

#
# Horizontal margin (left) for the menu
#
hmargin=2

#
# String corresponding to the 'current' vertical margin
#
str_vmargin="\n"

#
# String corresponding to the 'current' horizontal margin
#
str_hmargin="  "

#
# Clear path - devoid of any toolchain being considered.
#
path_clear=""

#
# Option corresponding to selected toolchain
#
menu_sel=0

#
# Path corresponding to the selected toolchain
#
path_sel=""

#
# Array of paths to available toolchains
#
declare -a tc_paths

#
# Array of labels associated with available toolchains
#
declare -a tc_labels

#
# Number of toolchains available
#
tc_num=0


# ==============================================================================
# Functions
# ==============================================================================

#
# Move cursor vertically - relative to current position.
#
function tp_cursor_relV
{
	local _delta=${1}
	local _code

	if [ ${_delta} -lt 0 ]; then
		#
		# Move up
		#
		_code=A

		_delta=$((_delta * -1))
	else
		#
		# Move down
		#
		_code=B
	fi

	echo -en "\e[${_delta}${_code}"
}

#
# Move cursor horizontally - relative to current position.
#
function tp_cursor_relH
{
	local _delta=${1}
	local _code

	if [ ${_delta} -lt 0 ]; then
		#
		# Move backward (left)
		#
		_code=D

		_delta=$((_delta * -1))
	else
		#
		# Move forward (right)
		#
		_code=C
	fi

	echo -en "\e[${_delta}${_code}"
}

#
# Erase from current cursor position to end-of-line
#
function tp_erase_eol
{
	echo -en "\e[0K"
}

#
# Set the vertical margin and associated string.
#
function tp_vmargin
{
	vmargin=${1}

	str_vmargin=""

	for (( _i=0; _i < vmargin; _i++ ))
	do
		str_vmargin+="\n"
	done
}

#
# Set the horizontal margin and associated string.
#
function tp_hmargin
{
	hmargin=${1}
	str_hmargin=""

	for (( _i=0; _i < hmargin; _i++ ))
	do
		str_hmargin+=" "
	done
}

#
# Show menu header
#
function tp_header
{
	local _str=${1}
	local _i

	echo -en "${str_vmargin}"

	echo -e "${str_hmargin}${CLR_HEAD}::${CLR_RESET}"
	echo -e "${str_hmargin}${CLR_HEAD}:: ${_str}${CLR_RESET}"
	echo -e "${str_hmargin}${CLR_HEAD}::${CLR_RESET}"
}

#
# Show menu footer
#
function tp_footer
{
	local _str=${1}
	local _i

	echo -e "${str_hmargin}${CLR_FOOT}::${CLR_RESET}"
	echo -e "${str_hmargin}${CLR_FOOT}:: ${_str}${CLR_RESET}"
	echo -e "${str_hmargin}${CLR_FOOT}::${CLR_RESET}"

	echo -e "${str_vmargin}"
}

#
# Show a menu option
#
function tp_option
{
	local _str=${1}

	echo -e "${str_hmargin}${CLR_MENU} : ${_str}${CLR_RESET}"
}

#
# Show the user prompt
#
function tp_prompt
{
	local _str=${1}

	echo -e "${str_hmargin}${CLR_PROMPT} : ${_str}${CLR_RESET}"
}

#
# Show help text
#
function tp_help
{
	local _str=${1}

	tp_erase_eol

	echo -e "${str_hmargin}${CLR_HELP} : ${_str}${CLR_RESET}"
}

#
# Show status message
#
function tp_status
{
	local _str=${1}

	tp_erase_eol

	echo -e "${str_hmargin}${CLR_STATUS} : ${_str}${CLR_RESET}"
}

#
# Show a 'success' string.
#
function tp_success
{
	local _str=${1}

	tp_erase_eol

	echo -e "${str_hmargin}${CLR_SUCCESS}:: ${_str}${CLR_RESET}"
}

#
# Show a 'failure' string.
#
function tp_failure
{
	local _str=${1}

	tp_erase_eol

	echo -e "${str_hmargin}${CLR_FAILURE}:: ${_str}${CLR_RESET}"
}

#
# Derive clear path - without any toolchain in consideration
#
function tp_clear_path
{
	local _ifs=${IFS}
	local _path=()

	local _dir
	local _tc
	local _flag

	#
	# Set IFS for parsing PATH
	#
	IFS=':'

	#
	# Extract components of PATH into an array.
	# Filter any path that exist in toolchain paths listed above.
	#
	read -ra _path <<< "${PATH}"

	for _dir in "${_path[@]}"; do
		_flag=0

		for _tc in "${tc_paths[@]}"; do
			if [ "${_dir}" == "${_tc}" ]; then
				_flag=1
			fi
		done

		if [ ${_flag} -eq 0 ]; then
			path_clear+="${_dir}:"
		fi
	done

	#
	# Remove trailing ":"
	#
	path_clear=${path_clear%?}

	#
	# Restore IFS
	#
	IFS=${_ifs}
}

#
# Show the menu
#
function tp_show_menu
{
	local _min=${1}
	local _max=${2}
	local _idx=1
	local _tc
	local _sel

	tp_option ""		# Spacer

	for _tc in "${tc_labels[@]}"; do
		tp_option "${_idx})  ${_tc}"

		_idx=$((_idx + 1))
	done

	tp_option ""		# Spacer

	tp_prompt "Your choice : "
	tp_help "(Enter number ${_min} thru ${_max} or 'q' to quit)"
}

#
# Get the user choice.
# Valid integer range is provided via arguments to this function
#
function tp_get_choice
{
	local _min=${1}
	local _max=${2}
	local _reg="^[0-9]+$"
	local _sel
	local _f=0

	while [ ${_f} -eq 0 ]; do
		read _sel

		if [ "${_sel}" == "q" ];then
			_f=2
		else
			if [[ ${_sel} =~ ${_reg} ]]; then
				if [ ${_sel} -ge ${_min} ] && [ ${_sel} -le ${_max} ]; then
					_f=1
				fi
			fi

			if [ ${_f} -eq 0 ]; then
				#
				# Position cursor to the prompt & erase to end-of-line
				#
				tp_cursor_relV -1
				tp_cursor_relH 19
				tp_erase_eol
			fi
		fi
	done

	if [ ${_f} -eq 1 ]; then
		#
		# Adjust the choice for array indexing.
		#
		menu_sel=$((_sel - 1))

		path_sel=${tc_paths[${menu_sel}]}

		return 0
	else
		return 1
	fi
}

#
# Update path with right toolchain
#
function tp_update_path
{
	#
	# Go to the designated status line
	#
	tp_cursor_relV 5

	#
	# Update status
	#
	tp_success "Selected - ${tc_labels[${menu_sel}]}"

	#
	# Jump over the footer.
	# Honour vertical margin as well.
	#
	echo -en "${str_vmargin}"

	#
	# Update environment
	#
	export PATH=${path_clear}:${path_sel}
}

#
# Quit without making any change
#
function tp_quit
{
	#
	# Go to the designated status line
	#
	tp_cursor_relV 5

	#
	# Update status
	#
	tp_status "Quitting without any change."

	#
	# Jump over the footer.
	# Honour vertical margin as well.
	#
	echo -en "${str_vmargin}"
}

#
# Handle Ctrl-C
#
function handle_ctrl_c
{
	#
	# Move silently to next line
	#
	echo ""

	#
	# Go to the designated status line
	#
	tp_cursor_relV 5

	#
	# Update status
	#
	tp_failure "Caught Ctrl-C!"

	#
	# Get ready for next character.
	#
	tp_cursor_relV -7
	tp_cursor_relH 19
	tp_erase_eol
}

#
# Read and parse the contents of configuration file.
# (Existence of file is already tested.)
#
function tp_read_cfg
{
	local _cfg=${1}
	local _str
	local _l
	local _p
	local _line=1

	while read _str
	do
		#
		# Skip empty strings
		#
		if [ ! -z "${_str}" ]; then
			#
			# Skip comments
			#
			if [[ ${_str:0:1} != '#' ]]; then

				_l=$(echo ${_str} | cut -s -d':' -f1)
				_p=$(echo ${_str} | cut -s -d':' -f2)

				if [ -z "${_l}" ] || [ -z "${_p}" ]; then
					tp_status "  - check syntax on line ${_line}"
				else
					tc_labels[${#tc_labels[*]}]=${_l}
					tc_paths[${#tc_paths[*]}]=${_p}
				fi
			fi
		fi

		_line=$(( _line + 1 ))

	done < ${_cfg}
}

# ==============================================================================
# BEGIN
# ==============================================================================

#
# Trap Ctrl-C
#
trap handle_ctrl_c SIGINT

#
# Set margins
#
tp_vmargin 1
tp_hmargin 2

tp_header "toolpick v${TP_VERSION}   ${TP_COPYRIGHT}"

if [ -f ${CFG_SYSTEM} ]; then
	tp_status "Reading ${CFG_SYSTEM}..."

	tp_read_cfg ${CFG_SYSTEM}
fi

if [ -f ${CFG_USER} ]; then
	tp_status "Reading ${CFG_USER}..."

	tp_read_cfg ${CFG_USER}
fi

if [ ${#tc_labels[@]} -eq ${#tc_paths[@]} ] && [ ${#tc_labels[@]} -gt 0 ]; then
	tc_num=${#tc_labels[@]}
fi

if [ ${tc_num} -gt 0 ]; then
	tp_clear_path

	tp_show_menu 1 ${tc_num}
else
	tp_failure "No toolchains to show :("
fi

tp_footer "${TP_LICENSE}"

#
# Read user's choice
#
if [ ${tc_num} -gt 0 ]; then
	tp_cursor_relV -7
	tp_cursor_relH 19

	tp_get_choice 1 ${tc_num}

	if [ ${?} -eq 0 ]; then
		tp_update_path
	else
		tp_quit
	fi
else
		tp_quit
fi

# ==============================================================================
# END
#
# Since this script would be sourced, avoid polluting user's environment with
# unnecessary variables and functions.
# ==============================================================================

#
# Clear the Ctrl-C handler
#
trap - SIGINT

#
# Clear function definitions
#
unset -f tp_cursor_relV
unset -f tp_cursor_relH
unset -f tp_erase_eol
unset -f tp_vmargin
unset -f tp_hmargin
unset -f tp_header
unset -f tp_footer
unset -f tp_option
unset -f tp_prompt
unset -f tp_help
unset -f tp_status
unset -f tp_success
unset -f tp_failure
unset -f tp_clear_path
unset -f tp_show_menu
unset -f tp_get_choice
unset -f tp_update_path
unset -f tp_quit
unset -f handle_ctrl_c
unset -f tp_read_cfg

#
# Clean variables
#
unset TP_VERSION
unset TP_COPYRIGHT
unset TP_LICENSE

unset CLR_HEAD
unset CLR_FOOT
unset CLR_MENU
unset CLR_PROMPT
unset CLR_HELP
unset CLR_SUCCESS
unset CLR_FAILURE
unset CLR_RESET

unset tc_labels
unset tc_paths
unset tc_num

unset CFG_SYSTEM
unset CFG_USER

unset vmargin
unset hmargin
unset str_vmargin
unset str_hmargin
unset path_clear
unset menu_sel
unset path_sel
