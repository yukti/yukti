#!/bin/bash
#
# yukti/annoqr/annoqr.sh
#
# This script generates annotated QR Codes from data in a file.
# See help below for the format of input file.
#
#
# The MIT License
#
# Copyright (c) 2015 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# ==============================================================================
# Constants
# ==============================================================================

#
# Version of the script
#
VERSION="1.00"

#
# Success
#
E_NONE=0

#
# No error. Help requested.
#
E_HELP=100

#
# Error: Dependencies not met.
#
E_DEPEND=101

#
# Error: Insufficient arguments
#
E_NARGS=102

#
# Error: Input file not found
#
E_FINPUT=103

#
# Error: File containing static info not found
#
E_FINFO=104

#
# Error: No target directory specified.
#
E_NOTARGET=105

#
# Error: Couldn't create target directory
#
E_TARGET=106

#
# Error: Static information is empty
#
E_INFO=107

#
# Error: Couldn't generate the image file
#
E_IMAGE=108


# ==============================================================================
# Variables
# ==============================================================================

#
# File to be used as input.
#
ArgInput=""

#
# Target directory where generated images get created.
#
ArgTarget=""

#
# Prefix to be used for generated filenames.
#
ArgPrefix=""

#
# Request help information
#
ArgHelp=0

#
# (Optional) Text file containing static information.
#
ArgInfo=""

#
# Static information read from the file.
#
StrInfo=""

# ==============================================================================
# Functions
# ==============================================================================

#
# Show header text
#
function showHeader
{
	echo "::"
	echo ":: annoqr.sh    (version ${VERSION})"
	echo "::"
	echo ":: Create annotated QR Codes."
	echo "::"
	echo ":: (c) 2015, Sanjeev Premi (spremi-at-ymail-dot-com)"
	echo ":: The MIT License (http://www.opensource.org/licenses/MIT)"
	echo "::"
}

#
# Show footer text
#
function showFooter
{
	local _str
	local _inf


	[ ${1} -eq ${E_HELP} ] && exit 0

	case ${1} in

	${E_NONE})
		_str="Done"
		;;

	${E_NARGS})
		_str="Insufficient arguments"
		;;

	${E_FINPUT})
		_str="Input file not found"
		_inf="(File: ${ArgInput})"
		;;

	${E_FINFO})
		_str="Info file not found"
		_inf="(File: ${ArgInfo})"
		;;

	${E_NOTARGET})
		_str="No target directory specified"
		;;

	${E_TARGET})
		_str="Couldn't create directory"
		_inf="(Directory: ${ArgTarget})"
		;;

	${E_INFO})
		_str="Static information is empty."
		_inf="(Check contents of: ${ArgInfo})"
		;;

	${E_IMAGE})
		_str="Couldn't create image"
		_inf="(File: ${ArgInfo})"
		;;

	${E_DEPEND})
		_str="Missing dependencies"
		;;

	*)
		_str="Encountered an error"
		;;
	esac

	if [ ${1} -eq ${E_NONE} ]; then
		echo "::"
		echo ":: ${_str} :)"
		echo "::"

		exit 0
	else
		echo "::"

		[ "${_str}" != "" ] && echo ":: ${_str} :("
		[ "${_inf}" != "" ] && echo ":: ${_inf}"

		echo "::"

		exit 1
	fi
}


#
# Show help message
#
function showHelp
{
	echo "::"
	echo ":: Usage:"
	echo "::"
	echo "::   ./annoqr.sh -f <file> -t <dir> [ -p <string> -i <file> ]"
	echo " :"
	echo " : where,"
	echo " :"
	echo " :   -f <file>"
	echo " :        Name of input file containing label and text"
	echo " :        for generating the QR Code."
	echo " :"
	echo " :   -t <dir>"
	echo " :        Directory where generated images will be stored."
	echo " :"
	echo " :   -p <string>"
	echo " :        Prefix to be used in filename of generated images."
	echo " :"
	echo " :   -i <file>"
	echo " :        File containing static information to be included"
	echo " :        in each generated image."
	echo " :"
	echo " : Each line in the input file must be in following format:"
	echo " :"
	echo " :     label : string to be encoded"
	echo " :"
	echo " : The label appears in plain text."
	echo " :"
	echo "::"
}

#
# Check whether an application exists
#
function appExists
{
	local _app=${1}
	local _pkg=${2}

	local _ret=$(which --skip-alias --skip-functions ${_app} 2> /dev/null)

	if [ "${_ret}" == "" ]; then
		echo ""
		echo "[E] Couldn't find '${_app}' in your path."
		echo "[E] Ensure that package '${_pkg}' is installed and accessible"
		echo "[E] in your PATH."
		echo ""

		return 1
	fi

	echo " : Found '${_app}'."

	return 0
}

#
# Check if a file is readable
#
function isReadable
{
	local _f=${1}

	if [ ! -r ${_f} ]; then
		echo ""
		echo "[E] Couldn't read '${_f}'."
		echo "[E] Ensure that name and/or path is correct."
		echo "[E] You may also want to check the file permissions."
		echo ""

		return 1
	fi

	return 0
}

#
# Prepare target directory
#
function prepareTarget
{
	local _d=${1}

	mkdir -p ${_d} 2> /dev/null

	if [ ${?} -ne 0 ]; then
		echo ""
		echo "[E] Couldn't create directory '${_d}'."
		echo "[E] Ensure that you have permissions to create it."
		echo ""

		return 1
	fi

	return 0
}


# ==============================================================================
# Begin
# ==============================================================================

showHeader

#
# Check whether 'qrencode' exists
#
appExists qrencode qrencode
[ ${?} -ne 0 ] && showFooter ${E_DEPEND}

#
# Check whether 'ImageMagick' exists
#
appExists mogrify ImageMagick
[ ${?} -ne 0 ] && showFooter ${E_DEPEND}

#
# Fetch command-line arguments
#
[ $# -eq 0 ] && showHelp && showFooter ${E_HELP}

while getopts ":f:i:t:p:h" opt; do
	case "${opt}" in
	'f')
		ArgInput=${OPTARG}
		;;
	't')
		ArgTarget=${OPTARG}
		;;
	'i')
		ArgInfo=${OPTARG}
		;;
	'p')
		ArgPrefix=${OPTARG}
		;;
	*)
		ArgHelp=1
		;;
	esac
done

#
# Show help - if required - before we moving ahead.
#
[ ${ArgHelp} -eq 1 ] && showHelp && showFooter ${E_HELP}

#
# Are input files readable?
#
[ "${ArgInput}" == "" ] && showHelp && showFooter ${E_HELP}

isReadable ${ArgInput}
[ ${?} -ne 0 ] && showFooter ${E_FINPUT}

if [ "${ArgInfo}" != "" ]; then
	isReadable ${ArgInfo}
	[ ${?} -ne 0 ] && showFooter ${E_FINFO}
fi

#
# Prepare target directory
#
[ "${ArgTarget}" == "" ] && showFooter ${E_NOTARGET}

prepareTarget ${ArgTarget}
[ ${?} -ne 0 ] && showFooter ${E_TARGET}

#
# Read static information in a variable
#
if [ "${ArgInfo}" != "" ]; then
	StrInfo=$(cat ${ArgInfo} | sed -e 's/^ *//' -e 's/ *$//')

	[ "${StrInfo}" == "" ] && showFooter ${E_INFO}
fi

#
# Show input parameters
#

echo ""
echo ":: Input file         - ${ArgInput}"
echo ":: Target directory   - ${ArgTarget}"
echo ":: "

if [ "${ArgPrefix}" == "" ]; then
	echo ":: File prefix        - NOT PROVIDED"
else
	echo ":: File prefix        - ${ArgPrefix}"
fi

echo ":: "

if [ "${StrInfo}" == "" ]; then
	echo ":: Static info        - NOT PROVIDED"
else
	echo ":: Static info        - ${StrInfo}"
fi

echo "::"
echo ""

#
# Process each line in the input file
#
echo ":: Generating QR Code"
echo "::"

count=0

while read line
do
	#
	# Split line. Extract label & text
	#
	label=$(echo ${line} | cut -f1 -d: | sed -e 's/^ *//' -e 's/ *$//')
	text=$(echo ${line} | cut -f2- -d: | sed -e 's/^ *//' -e 's/ *$//')

	#
	# Derive name of the image file
	#
	qrcfile=$(printf "%s/%s%03d.png" ${ArgTarget} ${ArgPrefix} ${count})

	echo " : ${qrcfile}"
	#
	# Generate QR Code
	#
	qrencode -o ${qrcfile} -s 10 -l M -m 5 -i "${text}"

	#
	# Add label
	#
	mogrify -gravity south -pointsize 20 -font Open-Sans -annotate +0+5 "${label}" ${qrcfile}

	#
	# Add info - if requested
	#
	if [ "${StrInfo}" != "" ]; then
		mogrify -gravity north -pointsize 10 -font Open-Sans -annotate +0+0 "${StrInfo}" ${qrcfile}
	fi

	count=$((count + 1))

done < ${ArgInput}

echo ""

# ==============================================================================
# End
# ==============================================================================
showFooter ${E_NONE}
